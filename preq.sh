#!/bin/bash
#
if [ ! -d "/opt/train-schedule" ]; then
  echo -e "[Unit]\\nDescription=Train Schedule\\nAfter=network.target\\n\\n[Service]\\n\\nType=simple\\nWorkingDirectory=/opt/train-schedule\\nExecStart=/usr/bin/node bin/www\\nStandardOutput=syslog\\nStandardError=syslog\\nRestart=on-failure\\n\\n[Install]\\nWantedBy=multi-user.target" > /etc/systemd/system/train-schedule.service
/usr/bin/systemctl daemon-reload
  mkdir -p /opt/train-schedule
  #chown root:train-schedule /opt/train-schedule
  chmod 775 /opt/train-schedule/
  yum -y install nodejs unzip
fi